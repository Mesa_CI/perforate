#!/usr/bin/env python3

import argparse
import multiprocessing
import os
import textwrap
import sys

PROJECT_DIR = os.path.abspath(f"{os.path.dirname(__file__)}/..")
sys.path.append(f"{PROJECT_DIR}/lib")
from benchmark import abn, run, clock, scores

class CpuStressor:
    def __init__(self):
        self._pid = None

    def start(self):
        self._pid = run(["stress", "-c", str(int(multiprocessing.cpu_count() / 2))],
                         background=True)

    def stop(self):
        self._pid.kill()

def main():
    """run unigine valley, and determine the max gpu clock at TDP"""
    parser = argparse.ArgumentParser(description=
                                     'Run unigine valley to determine gpu frequency at TDP.',
                                     formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument('--target', type=str, help='mesa install location')
    parser.add_argument('--display', type=str,
                        help=textwrap.dedent('''\
                        [headless|weston|xorg|native]
                            headless: launch weston headless
                            weston: stop gdm and launch weston on the host
                            xorg: stop gdm and launch xorg on the host
                            native (default): use existing desktop environment'''))
    parser.add_argument('--score_host', type=str, help='host on which perforate sql db resides')

    cpu_load = CpuStressor()
    cpu_load.start()
    freq_monitor = clock.ThreadedFreqMonitor(0.5)
    freq_monitor.start()

    args = parser.parse_args()
    abn.mount_abn("valley")
    abn.mount_benchmark("valley")
    abn.run_benchmark("valley",
                      before=None,
                      after=None,
                      target=args.target,
		      iterations=1,
                      display=args.display)
    freq_monitor.stop()
    cpu_load.stop()

    scores.Scores(score_host=args.score_host).set_gpu_frequency_limit(
        os.environ.get("NODE_NAME"),
        freq_monitor.max_stable_freq()
    )


if __name__ == "__main__":
    main()

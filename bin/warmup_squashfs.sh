#!/bin/bash

echo "127.0.0.1 `hostname`" | sudo tee -a /etc/hosts > /dev/null 2>&1
for image in "$@"
do
    sudo mkdir -p /squashfs/$image
    sudo mount -t squashfs -o loop /var/squash/$image /squashfs/$image || echo "warmup needed for $image"
    sleep 1
    ls /squashfs/$image > /dev/null
done

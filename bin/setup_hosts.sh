#!/bin/bash

echo "Setting up hosts"
echo "127.0.0.1 `hostname`" | sudo tee -a /etc/hosts > /dev/null 2>&1
echo "0.0.0.0 cdn.cloudflare.steamstatic.com" | sudo tee -a /etc/hosts > /dev/null
echo  "0.0.0.0 client-update.akamai.steamstatic.com" | sudo tee -a /etc/hosts > /dev/null
echo  "0.0.0.0 cdn.steamstatic.com" | sudo tee -a /etc/hosts > /dev/null
echo  "10.54.30.11 mesa-ci-perforate.local mesa-ci-perforate.jf.intel.com" | sudo tee -a /etc/hosts > /dev/null
echo  "10.54.30.105 mesa-ci-files.local mesa-ci-files.jf.intel.com" | sudo tee -a /etc/hosts > /dev/null
echo  "10.54.30.101 mesa-ci-jenkins.local mesa-ci-jenkins.jf.intel.com" | sudo tee -a /etc/hosts > /dev/null
echo  "10.54.30.106 otc-mesa-ci.local otc-mesa-ci.jf.intel.com" | sudo tee -a /etc/hosts > /dev/null

#!/usr/bin/env bash

SERVER=$1
rsync /scores/scores.json rsync://$SERVER/rsync/`date -Iseconds`.json && rm /scores/scores.json || echo "no score to publish"

#!/usr/bin/env python3
import argparse
import os
import sys
import textwrap

DOCKER_DIR = os.path.abspath(f"{os.path.dirname(__file__)}/../docker")
sys.path.append(f"{DOCKER_DIR}/files/scripts")
from benchmark import run

def main():
    """start steam for game installation

    run this script to prepare steam for a particular game.  After
    closing steam, the container will be committed to a tag name.

    Steps to install game
     - put steam in online mode
     - install game
     - launch game manually
     - exit game
     - put steam in offline mode
     - disable cloud shader cache
     - run ABN for the workload from the command line with --artifacts.download
     - quit steam

    Then commit the container name to the desired tag.
    """

    parser = argparse.ArgumentParser(description='Launch steam for game installation and '
                                     'benchmark initialization.',
                                     formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument('benchmark', type=str, help='workload to run (dota, csgo, etc)')
    parser.add_argument('--display', type=str,
                        help=textwrap.dedent('''\
                        [headless|weston|xorg|native]
                            headless: launch weston headless
                            weston: stop gdm and launch weston on the host
                            xorg: stop gdm and launch xorg on the host
                            native (default): use existing desktop environment'''))
    parser.add_argument('--version', type=str,
                        help="version string for saved image name")
    args = parser.parse_args()

    run([f"{DOCKER_DIR}/docker-compose.sh",
         "--project-directory", DOCKER_DIR, "run",
         "--name", args.benchmark,
         "execute",
         "/scripts/install_steam_game.py"])

if __name__ == "__main__":
    main()

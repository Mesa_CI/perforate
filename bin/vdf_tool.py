#!/usr/bin/env python3

import argparse
import os
import vdf

parser = argparse.ArgumentParser(description="update keys in steam vdf files ")
parser.add_argument('--key', type=str, required=True, help="path to option, separated by '/'")
parser.add_argument('--value', type=str, required=True, help="value to set")
parser.add_argument('vdf_file')

args = parser.parse_args()
assert os.path.exists(args.vdf_file)
conf = vdf.load(open(str(args.vdf_file), encoding='utf8'))

value = conf
keys = args.key.split('/')
for key in keys[0:-1]:
    value = value[key]

assert value
value[keys[-1]] = args.value

vdf.dump(conf, open(str(args.vdf_file), 'w', encoding='utf8'), pretty=True)

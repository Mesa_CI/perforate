#!/usr/bin/env python3

import argparse
import os
import sys

sys.path.append(f"{os.path.dirname(__file__)}/../lib")

from benchmark import mesa
from benchmark import capture_commands

def main():
    parser = argparse.ArgumentParser(description='Benchmark a mesa commit on the target workload.')
    parser.add_argument('--before', type=str, help='git sha of the BEFORE commit')
    parser.add_argument('--target', type=str, help='git sha of the TARGET commit')
    parser.add_argument('--after', type=str, help='git sha of the AFTER commit')
    parser.add_argument('-v', '--verbose', action='store_true')
    args = parser.parse_args()
    if args.verbose:
        capture_commands("/var/log/perforate/build_command_log.sh")
    src_dir = "/home/perforate/mesa"
    mesa.clone(src_dir)
    mesa.clean(src_dir)
    targets = []
    if args.before:
        targets.append(("before", args.before))
    if args.target:
        targets.append(("target", args.target))
    if args.after:
        targets.append(("after", args.after))

    for (target, commit) in targets:
        mesa.fetch(src_dir, commit)

    if not args.target:
        assert args.before and args.after
        target = mesa.bisect_commit(src_dir, args.before, args.after)
        print(f"INFO: selecting {target} as target via bisection")
        targets.append(("target", target))

    for (target, commit) in targets:
        mesa.checkout(src_dir, commit)
        mesa.build(src_dir, target)

if __name__ == "__main__":
    main()

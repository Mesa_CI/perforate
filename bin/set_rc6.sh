#!/bin/bash

# Toggle RC6 availability. RC6 is the deepest GPU sleep state.
# Disabling RC6 may avoid time consuming sleep wakeup, but consume
# more power.

# Usage:
#   set_rc6.sh off       #force rc6 off
#   set_rc6.sh on        #allow rc6 on (default)

function usage {
    echo "USAGE set_rc6.sh on|off"
    exit 1
}

function get_rc6_job_pid {
  echo `ps aux | grep "sleep 0xFFFFFFFF$" | awk '{print $2}'`
}

# Error unless running as su or have passwordless sudo
su_whoami=$(SUDO_ASKPASS=/bin/false sudo -A whoami 2>/dev/null)
if [ "$su_whoami" != "root" ]; then
  echo "ERROR: need to run as root"
  exit 1
fi

if [ "$#" -ne 1 ]; then
    usage
fi

# rc6 set by argument
if [ "$1" == "on" ]; then
    disable_rc6=0
elif [ "$1" == "off" ]; then
    disable_rc6=1
else
    usage
fi

if [ -e /dev/dri/card0 ]; then
    forcewake=/sys/kernel/debug/dri/0/i915_forcewake_user
else
    forcewake=/sys/kernel/debug/dri/1/i915_forcewake_user
fi

if [ $disable_rc6 -ne 0 ]; then
  nohup sleep 0xFFFFFFFF 2>/dev/null | sudo tee $forcewake &
  if [[ -z $(get_rc6_job_pid) ]]; then
      echo Error: Failed to disable RC6
      exit 1
  fi
  echo "RC6 disabled"
  exit 0
fi

# else
sudo kill $(get_rc6_job_pid) 2>/dev/null
if [[ -z $(get_rc6_job_pid) ]]; then
    echo "RC6 enabled (default)"
else
    echo Error: Failed to enable RC6
fi


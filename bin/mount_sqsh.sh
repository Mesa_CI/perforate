#!/bin/bash

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

echo "Mounting $1"
SQUASH_FILE=$1.sqsh
if [ $3 ]; then
    SQUASH_FILE=$3
fi

sudo mkdir -p /squashfs/$1/{overlay,ro}
sudo mount -t tmpfs tmpfs /squashfs/$1/overlay
sudo mkdir -p /squashfs/$1/overlay/{up,work}
sudo mount -t squashfs -o loop /var/squash/$SQUASH_FILE /squashfs/$1/ro

if [ ! -d $2 ]; then
    sudo mkdir -p $2
fi

sudo mount -t overlay overlay -o lowerdir=/squashfs/$1/ro,upperdir=/squashfs/$1/overlay/up,workdir=/squashfs/$1/overlay/work $2

sudo chown perforate:perforate $2



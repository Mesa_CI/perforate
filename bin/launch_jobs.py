import os
import sys

sys.path.append(f"{os.path.dirname(__file__)}/../lib")
from jenkins import Jenkins, Affinity
from benchmark.scores import Scores

def main():
    """
     - determine which perf systems are idle
     - associate a perf target with each system
     - launch jobs
    """
    jenkins = Jenkins()
    affinity = Affinity()
    for system, platform in jenkins.get_idle_sytems().items():
        benchmarks = affinity.benchmarks_for_device(system, platform)
        benchmark = Scores('mesa-ci-perforate.local').choose_benchmark(system, benchmarks)
        print(f"launch {benchmark} on {system}")
        params = { "benchmark" : benchmark,
                   "before" : "None",
                   "target" : "None",
                   "after" : "None",
                   "label" : system,
                   }
        options = "&".join([f"{k}={v}" for k, v in params.items()])
        url = ("http://mesa-ci-jenkins.jf.intel.com/job/public/job/perforate/"
               f"buildWithParameters?{options}")
        jenkins.start_job(url)

if __name__ == "__main__":
    main()

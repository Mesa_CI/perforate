#!/usr/bin/env python3

import argparse
import base64
import glob
import os
import shutil
import sys
import textwrap
import time
import socket
from urllib.request import Request, urlopen

PROJECT_DIR = os.path.abspath(f"{os.path.dirname(__file__)}/..")
PKG_ARCHIVE_DIR = f"{PROJECT_DIR}/var/pkg_archives"
DOCKER_DIR = f"{PROJECT_DIR}/docker/benchmark"
sys.path.append(f"{PROJECT_DIR}/lib")
from benchmark import run, capture_commands, scores, abn, mesa

BENCHMARK_IMAGES = {"dota" : ["abn.2.6.1109.sqsh"],
                    "sotr" : ["abn.sqsh"],
                    "superposition" : ["abn.sqsh", "superposition.sqsh"],
                    "valley" : ["abn.sqsh", "valley.sqsh"],
                    }
DOCKER_ARGS = ["-e",
               f"SQL_DATABASE_USER={os.environ.get('SQL_DATABASE_USER')}",
               "-e",
               f"SQL_DATABASE_PW={os.environ.get('SQL_DATABASE_PW')}",
               "-e", f"NODE_NAME={os.environ.get('NODE_NAME')}",
               "-v", (f"{os.environ.get('PERFORATE_IPTABLES_RULES')}:"
                      "/tmp/setup_docker_network.sh"),
               "-v", f"{PKG_ARCHIVE_DIR}:/tmp/pkgs"]

for traci_bench in abn.TRACI_BENCHMARKS:
    BENCHMARK_IMAGES[traci_bench] = ["traci.sqsh"]

def _build_mesa(before, target, after, verbose):
    run(["docker-compose",
         "--project-directory", DOCKER_DIR,
         "pull", "build"])
    cmd = [f"{DOCKER_DIR}/docker-compose.sh",
           "--project-directory", DOCKER_DIR,
           "run", "--rm", "build",
           "/usr/local/bin/build_mesa.py"]
    if before:
        cmd += ["--before", before]
    if target:
        cmd += ["--target", target]
    if after:
        cmd += ["--after", after]
    if verbose:
        cmd.append("-v")
    run(cmd, verbose=True)

def _find_max_stable_gpu_freq_at_tdp(target, display, score_host):
    if "NODE_NAME" not in os.environ:
        return
    if scores.Scores(score_host=score_host).get_gpu_frequency_limit(
        os.environ.get("NODE_NAME")
    ):
        return

    # else run a benchmark to set a limit
    _fetch_benchmark("valley")
    _warmup_squash_mount("valley")
    cmd = [f"{DOCKER_DIR}/docker-compose.sh",
           "--project-directory", DOCKER_DIR,
           "run", "--rm"]
    cmd += DOCKER_ARGS
    cmd += [_image_for_benchmark("valley"),
            "/tmp/setup_docker_network.sh",
            "/usr/local/bin/find_max_stable_gpu_freq_at_tdp.py",
            "--target", target, "--display", display,
            "--score_host", score_host]
    run(cmd, verbose=True)

def _image_for_benchmark(benchmark):
    docker_images = { "sotr" : "sotr",
                      "dota" : "dota",
                     }
    if benchmark in abn.TRACI_BENCHMARKS:
        return "traci"
    if benchmark not in docker_images:
        return "execute"
    return docker_images[benchmark]

def _fetch_docker_pkg_files():
    os.makedirs(PKG_ARCHIVE_DIR, exist_ok=True)
    cmd = ["rsync",
           "--progress", "-t",
           "otc-mesa-ci.jf.intel.com::nfs/"
           "results/perforate/pkgs/iptables_pkgs.tar",
           PKG_ARCHIVE_DIR]
    run(cmd)

def _fetch_benchmark(benchmark):
    squash_dir = f"{PROJECT_DIR}/var/squash"
    trace_dir = f"{PROJECT_DIR}/var/traces"
    fossil_dir = f"{PROJECT_DIR}/var/fossils"
    image_details =  {"abn.sqsh" : { "size" : 145833984, "time" : 1691536956},
                      'abn.2.6.1109.sqsh' : { "size" : 98295808, "time" : 1710882651},
                      "csgo.sqsh" : { "size" : 21493006336, "time" : 1694039078},
                      "dota.sqsh" : { "size" : 32168812544, "time" : 1700265098},
                      "superposition.sqsh" : {"size" : 1715646464, "time" : 1690498755 },
                      "valley.sqsh" : {"size" : 413630464, "time" : 1702936882 },
                      "traci.sqsh" : {"size" : 1834823680, "time" : 1713566197 }
                      }

    needed_images = []
    required_space = 0
    for image in BENCHMARK_IMAGES[benchmark]:
        image_path = f"{squash_dir}/{image}"
        if os.path.exists(image_path):
            if (int(os.path.getmtime(image_path)) == image_details[image]["time"] and
                os.path.getsize(image_path) == image_details[image]["size"]):
                continue
            os.rename(image_path, f"{image_path}_{int(time.time())}")
        needed_images.append(image)
        required_space += image_details[image]["size"]
    if _image_for_benchmark(benchmark) != "execute":
        # docker images with games are 50G
        required_space += 50 * pow(10, 9)

    os.makedirs(squash_dir, exist_ok=True)
    _, _, free_space = shutil.disk_usage(squash_dir)
    if required_space * 1.3 > free_space:
        bytes_per_gig = 1024 ^ 3
        free_gb = int(free_space / bytes_per_gig)
        print(f"warn: disk space low: {free_gb} GB")
        for old_image in glob.glob(f"{squash_dir}/*.sqsh*"):
            os.unlink(old_image)
        for old_trace in glob.glob(f"{trace_dir}/*.lcs2"):
            os.unlink(old_trace)
        for old_fossil in glob.glob(f"{trace_dir}/*-*"):
            os.unlink(old_fossil)
        needed_images = BENCHMARK_IMAGES[benchmark]
        run(["docker", "system", "prune", "-af"])
        _, _, free_space = shutil.disk_usage(squash_dir)
    if needed_images:
        if required_space * 1.3 > free_space:
            assert required_space * 1.3 < free_space
        for image in needed_images:
            print(f"INFO: fetching {image}")
            cmd = ["rsync",
                   "--progress", "-t",
                   f"otc-mesa-ci.jf.intel.com::nfs/results/perforate/squash/{image}",
                   squash_dir]
            run(cmd)
    run(["docker-compose",
         "--project-directory", DOCKER_DIR,
         "pull", _image_for_benchmark(benchmark)])

def _warmup_squash_mount(benchmark):
    """After boot, systems will initially fail to mount an additional
    loop device in docker.  Subsequent docker containers will
    successfully mount the additional loop device.  To work around
    this docker bug, start a container to unsuccessfully mount all
    squash file systems and exit.
    """
    for _ in BENCHMARK_IMAGES[benchmark]:
        run([f"{DOCKER_DIR}/docker-compose.sh",
             "--project-directory", DOCKER_DIR,
             "run", "--rm", "execute",
             "/usr/local/bin/warmup_squashfs.sh"] +
            BENCHMARK_IMAGES[benchmark],
            check=False)

def _execute_benchmark(benchmark, iterations, before, target, after, verbose, display):
    cmd = [f"{DOCKER_DIR}/docker-compose.sh",
           "--project-directory", DOCKER_DIR,
           "run", "--rm"]
    cmd += DOCKER_ARGS
    cmd += [_image_for_benchmark(benchmark),
            "/tmp/setup_docker_network.sh",
            "/usr/local/bin/perforate.py",
            "-n", str(iterations)]
    if before:
        cmd += ["-a", before]
    if target:
        cmd += ["-b", target]
    if after:
        cmd += ["-c", after]
    if verbose:
        cmd.append("-v")
    cmd += ["--display", display]
    cmd.append(benchmark)
    run(cmd, verbose=True)

def _publish_scores(score_host):
    if not score_host:
        return
    run(["docker-compose",
         "--project-directory", DOCKER_DIR,
         "pull", "publish"])
    cmd = [f"{DOCKER_DIR}/docker-compose.sh",
           "--project-directory", DOCKER_DIR,
           "run", "--rm", "publish",
           "/usr/local/bin/publish_scores.sh", score_host]
    run(cmd, verbose=True)

def docker_main():
    """parse args and invoke the benchmark"""

    parser = argparse.ArgumentParser(description='Benchmark a mesa commit on the target workload.')
    parser.add_argument('benchmark', type=str, help='workload to run (dota, csgo, etc)')
    parser.add_argument('-a', type=str, help='git sha of the BEFORE commit')
    parser.add_argument('-b', type=str, help='git sha of the TARGET commit')
    parser.add_argument('-c', type=str, help='git sha of the AFTER commit')
    parser.add_argument('-n', type=int, help='iterations to test')
    parser.add_argument('-v', '--verbose', action='store_true')
    parser.add_argument('--display', type=str,
                        help=textwrap.dedent('''\
                        [headless|weston|xorg|native]
                            headless: launch weston headless
                            weston: stop gdm and launch weston on the host
                            xorg: stop gdm and launch xorg on the host
                            native (default): use existing desktop environment'''))
    args = parser.parse_args()
    if not args.b:
        if args.a and args.c:
            args.b = mesa.bisect_commit("/home/perforate/mesa", args.a, args.c)
        else:
            print("error: target commit required.  Pass -b to this script")
            parser.print_help()
            sys.exit(-1)
    if args.benchmark not in abn.BENCHMARKS:
        print(f"error: invalid benchmark. Choose one of {', '.join(abn.BENCHMARKS)}")
        parser.print_help()
        sys.exit(-1)
    display = "native"
    if args.display:
        display = args.display
    if display == "weston":
        print("WARN: weston display not yet supported")
        display = "native"
    elif display not in ["native", "headless", "xorg"]:
        print(f"ERROR: invalid --display: {display}")
        parser.print_help()
        sys.exit(-1)

    if args.verbose:
        capture_commands("/var/log/perforate/benchmark_command_log.sh")

    abn.mount_abn(args.benchmark)
    abn.mount_benchmark(args.benchmark)

    abn.run_benchmark(args.benchmark,
                      before=args.a,
                      target=args.b,
                      after=args.c,
		      iterations=args.n,
                      display=display)

def host_main():
    """parse args and invoke the benchmark"""

    parser = argparse.ArgumentParser(description='Benchmark a mesa commit on the target workload.',
                                     formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument('benchmark', type=str, help='workload to run (dota, csgo, etc)')
    parser.add_argument('-a', type=str, help='git sha of the BEFORE commit')
    parser.add_argument('-b', type=str, help='git sha of the TARGET commit')
    parser.add_argument('-c', type=str, help='git sha of the AFTER commit')
    parser.add_argument('-n', type=int, help='iterations to test')
    parser.add_argument('--display', type=str,
                        help=textwrap.dedent('''\
                        [headless|weston|xorg|native]
                            headless: launch weston headless
                            weston: stop gdm and launch weston on the host
                            xorg: stop gdm and launch xorg on the host
                            native (default): use existing desktop environment'''))
    parser.add_argument('-v', '--verbose', action='store_true')
    parser.add_argument('--score_host', type=str, help='score database server')
    args = parser.parse_args()

    if args.a == "None":
        args.a = None
    if args.b == "None":
        args.b = None
    if args.c == "None":
        args.c = None

    if args.verbose:
        capture_commands(f"{PROJECT_DIR}/var/log/command_log.sh")
    if args.benchmark not in abn.BENCHMARKS:
        print(f"error: invalid benchmark. Choose one of {', '.join(abn.BENCHMARKS)}")
        parser.print_help()
        sys.exit(-1)
    display = "native"
    if args.display:
        display = args.display
    if display == "weston":
        print("WARN: weston display not yet supported")
        display = "native"
    elif display not in ["native", "headless", "xorg"]:
        print(f"ERROR: invalid --display: {display}")
        parser.print_help()
        sys.exit(-1)

    score_host = "mesa-ci-perforate.local"
    if args.score_host:
        score_host = args.score_host
        if score_host.lower() == "none":
            score_host = None

    iterations = 1
    if args.n:
        iterations = args.n

    old_scores = scores.Scores(score_host=score_host)

    if not (args.a or args.b or args.c):
        (args.a, args.c) = old_scores.longest_gap(args.benchmark)
        if args.a == args.c:
            # main,main returned if the newest score is older than 24hrs
            args.a = None
            args.b = "main"
            args.c = None

    old_scores.query(args.benchmark, [args.a, args.b, args.c])
    old_scores.write(f"{PROJECT_DIR}/var/results/old_scores.json")

    _build_mesa(before=args.a,
               target=args.b,
               after=args.c,
               verbose=args.verbose)

    _fetch_docker_pkg_files()
    _find_max_stable_gpu_freq_at_tdp(args.a or args.b or args.c, display, score_host)

    _fetch_benchmark(args.benchmark)
    _warmup_squash_mount(args.benchmark)
    _execute_benchmark(args.benchmark,
                      iterations=iterations,
                      before=args.a,
                      target=args.b,
                      after=args.c,
                      verbose=args.verbose,
                      display=display)

    _publish_scores(score_host=score_host)


if __name__ == "__main__":
    if os.path.exists("/usr/local/share/abn/dota.patch"):
        docker_main()
    else:
        host_main()

import base64
import getpass
import json
import os
import random
from urllib.request import urlopen, Request

from benchmark import abn

class Affinity:
    """Associates test targets with specific devices.  Affinity is
    preserved by saving the associations in a json file within the
    workspace."""
    def __init__(self, localstore=None):
        if not localstore:
            # structure of json file:
            # {"adl-05": {
            #     "benchmarks": ["sotr", "hogwarts"], "platform": "adl"
            #     },
            # }
            self._localstore= f"{os.path.dirname(__file__)}/.affinity.json"

    def benchmarks_for_device(self, device, platform):
        """Choose the best benchmarks to run on a device"""
        workloads_per_device = 5
        if os.path.exists(self._localstore):
            with open(self._localstore, "r", encoding="utf8") as inf:
                affinity = json.load(inf)
        else:
            affinity = {}
        if device in affinity:
            if len(affinity[device]["benchmarks"]) >= workloads_per_device:
                return affinity[device]["benchmarks"]
        else:
            affinity[device] = { "benchmarks": [], "platform": platform }
        benchmarks = list(abn.BENCHMARKS)
        random.shuffle(benchmarks)
        device_count = { b : 0 for b in benchmarks if (b not in abn.TRACI_BLACKLIST or
                                                       platform not in abn.TRACI_BLACKLIST[b]) }
        # find the benchmark with the fewest devices
        for _, info in affinity.items():
            if info["platform"] != platform:
                continue
            for benchmark in info["benchmarks"]:
                if benchmark not in device_count:
                    # benchmark was blacklisted from the platform
                    continue
                device_count[benchmark] += 1
        benchmark = min(device_count, key=device_count.get)
        affinity[device]["benchmarks"].append(benchmark)
        with open(self._localstore, "w", encoding="utf8") as outf:
            json.dump(affinity, outf)
        return affinity[device]["benchmarks"]

class Jenkins:
    """Determines idle systems on the CI and invokes jobs to test commits
    """
    def __init__(self):
        self._jenkins_host = "mesa-ci-jenkins.jf.intel.com"

        self._user = os.getenv("JENKINS_USER")
        self._token = os.getenv("JENKINS_PW")
        if not self._user:
            self._user = getpass.getuser()
        if not self._token:
            self._token = getpass.getpass()

        self._platforms = ["adl", "dg2", "icl"]
        self._devices = {}

    def _authenticate_request(self, request):
        base64string = base64.b64encode(bytes(f'{self._user}:{self._token}','ascii'))
        request.add_header("Authorization", f"Basic {base64string.decode('utf-8')}")
        return request

    def start_job(self, url):
        """starts a job on jenkins"""
        request = self._authenticate_request(Request(url, method="POST"))
        return urlopen(request)

    def get_idle_sytems(self):
        """returns a map of idle machines {machine_name : platform_label }
        """
        url = f"http://{self._jenkins_host}/computer/api/json"
        request = self._authenticate_request(Request(url))
        resp = json.loads(urlopen(request).read())
        idle_systems = {}
        for computer in resp["computer"]:
            if not computer["idle"]:
                continue
            if computer["offline"]:
                continue
            if computer["temporarilyOffline"]:
                continue
            is_perf = False
            platform = None
            for label in computer["assignedLabels"]:
                if label["name"] in self._platforms:
                    platform = label["name"]
                if label["name"] == "perf":
                    is_perf = True
            if not is_perf:
                continue
            if not platform:
                continue
            idle_systems[computer["displayName"]] = platform
        return idle_systems

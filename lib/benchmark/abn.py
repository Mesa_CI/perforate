import csv
import glob
import json
import os
import shutil
import subprocess
import time

import git

from .clock import ThreadedFreqMonitor, limit_gpu_frequency, get_card_file
from .command import run
from .display import start_headless, start_x, stop_x
from .scores import Scores

PROJECT_ROOT = os.path.abspath(f"{os.path.dirname(__loader__.path)}/../../")

ABN_BENCHMARKS = ["sotr", "dota", "superposition", "valley"]
TRACI_BENCHMARKS = {
    # fails on dg2 (dx12?)
    # 'assassinscreed': 'AssassinsCreedValhalla-trace-dx12-1080p-med',
    'baldursgate': 'BaldursGate3-trace-dx11-1440p-ultra',
    'blackops' : "Blackops3-trace-dx11-1080p-high",
    'borderlands': 'Borderlands3-trace-dx11-2160p-ultra',
    'citiesskylines': 'CitiesSkylines2-trace-dx11-1440p-high',
    'control': 'Control-trace-dx11-1440p-high',
    'cyberpunk': 'Cyberpunk-trace-dx12-1080p-high',
    # Bungie sucks, we will never support their titles
    # https://news.itsfoss.com/destiny-2-linux-bungie/
    # 'destiny': 'Destiny2-trace-dx11-1440p-highest',
    'factorio': 'Factorio-trace-1080p-high',
    'finalfantasy': 'FinalFantasy14-trace-dx11-1080p-high',
    'fortnite': 'Fortnite-trace-dx11-2160p-epix',
    'ghostrunner': 'Ghostrunner2-trace-dx11-1440p-ultra',
    'godofwar': 'GodOfWar-trace-dx11-2160p-ultra',
    'gta': 'GtaV-trace-dx11-2160p-ultra',
    'hitman': 'Hitman3-trace-dx12-1080p-med',
    'hogwarts': 'HogwartsLegacy-trace-dx12-1080p-ultra',
    'metroexodus': 'MetroExodus-trace-dx11-2160p-ultra',
    'mountandblade': 'MountAndBlade2-trace-dx11-1440p-veryhigh',
    'naraka': 'Naraka-trace-dx11-1440p-highest',
    'nba2k23': 'Nba2K23-trace-dx11-2160p-ultra',
    'palworld': 'Palworld-trace-dx11-1080p-med',
    'payday3': 'Payday3-trace-dx11-1440p-ultra',
    'pubg': 'PubG-trace-dx11-1440p-ultra',
    'spaceengineers': 'SpaceEngineers-trace-dx11-2160p-high',
    'spiderman': 'SpidermanRemastered-trace-dx12-1080p-med',
    'terminator': 'TerminatorResistance-trace-dx11-2160p-ultra',
    'totalwar': 'TotalWarPharaoh-trace-dx11-1440p-ultra',
    'warhammer': "TotalWarWarhammer3-trace-dx11-1080p-high",
    'witcher': 'Witcher3-trace-dx12-1080p-med',
}

TRACI_BLACKLIST = {
    # gpu hang, timeout
    "citiesskylines" : ['icl', 'dg2'],
    "metroexodus" : ['icl'],
    # timeout
    'borderlands' : ['icl'],
    'control': ['icl', 'adl'],
    'fortnite': ['icl'],
    'ghostrunner': ['icl'],
    'godofwar': ['icl'],
    'hitman': ['icl', 'adl'],
    'hogwarts': ['dg2', 'icl'],
    'mountandblade': ['icl', 'adl'],
    'nba2k23': ['adl'],
    'palworld': ['icl'],
    'spaceengineers': ['dg2'],
    'spiderman': ['icl', 'adl'],
    'terminator': ['icl'],
    'totalwar': ['icl'],

    # compiler infinite loop
    'gta': ['icl'],
}

BENCHMARKS = ABN_BENCHMARKS + list(TRACI_BENCHMARKS.keys())

def setup_hosts():
    run(["/usr/local/bin/setup_hosts.sh"])

def mount_squash(sqsh, mount_point, sqsh_file=None):
    if not sqsh_file:
        run(["/usr/local/bin/mount_sqsh.sh", sqsh, mount_point])
    else:
        run(["/usr/local/bin/mount_sqsh.sh", sqsh, mount_point, sqsh_file])

def mount_traci():
    mount_squash("traci", '/traci', None)
    run(["rm", '-r', '/traci/tools/traci/out/traces'])
    run(["rm", '-r', '/traci/tools/traci/out/fossils'])
    run(["ln", "-s", "/var/traces", "/traci/tools/traci/out/"])
    run(["ln", "-s", "/var/fossils", "/traci/tools/traci/out/"])

def mount_abn(benchmark):
    abn_files = {"dota" : "abn.2.6.1109.sqsh" }
    sqsh_file = None
    if benchmark in abn_files:
        sqsh_file = abn_files[benchmark]
    setup_hosts()
    if benchmark in ABN_BENCHMARKS:
        mount_squash('abn', '/abn', sqsh_file)
        patches = {'dota.patch' : '/abn/src/scripts/linux/perf3d/Dota2.py',
                   'sotr.patch' : '/abn/src/scripts/linux/perf3d/ShadowoftheTombRaider.py',
                   'valley.patch' : '/abn/src/scripts/linux/perf3d/unigineValley.py'
                   }
        for patch, target in patches.items():
            run(['patch', target, f"{PROJECT_ROOT}/share/abn/{patch}"])

    if benchmark in TRACI_BENCHMARKS:
        mount_traci()

    results_dir = "/home/perforate/results"
    if not os.path.exists(results_dir):
        run(["mkdir", "-p", results_dir])
    run(["sudo", "chown", "perforate:perforate", "/scores"])

def fix_sotr_prefs():
    pref_file = ("/home/perforate/.local/share/feral-interactive"
                 "/Shadow of the Tomb Raider/preferences")
    shutil.copy(pref_file, f"{pref_file}.bak")

    for build_to_use in ["target", "before", "after"]:
        try:
            dev_info = run(["sudo", "/usr/local/bin/with-mesa", build_to_use, "intel_dev_info"],
                           capture_output=True)
        except subprocess.CalledProcessError as err:
            print(f"WARN: could not run intel_dev_info: {err}")
            continue

    info = {}
    for line in str(dev_info.stdout, encoding="utf8").splitlines():
        if ':' not in line:
            continue
        k,v = line.split(':')[0:2]
        info[k.strip()] = v.strip()
    # strip trailing ({family}) from name
    name = info["name"]
    name = name[0:name.rfind('(')-1]
    pci_id = int(info["PCI device id"], 16)
    pci_domain = int(info["PCI domain"], 16)
    pci_bus = int(info["PCI bus"], 16)
    pci_dev = int(info["PCI dev"], 16)
    pci_rev = int(info["PCI revision id"], 16)
    cardrender = (f"{name} Vulkan ANV "
                  f"({pci_id:04x}8086,{pci_rev:x},{pci_dev*16*16 + pci_bus:x},{pci_domain:x})")
    regex = rf's/Intel\(R\) Arc\(tm\) A750 Graphics Vulkan ANV \(56a18086,8,3,0\)/{cardrender}/g'
    run(["sed", "--in-place", "-r",
         "-e", regex, pref_file])

def mount_benchmark(benchmark):
    mountpoints = {'superposition' : '/abn/artifacts',
                   'valley' : '/abn/artifacts/binaries',
                   }
    if benchmark in mountpoints:
        mount_squash(benchmark, mountpoints[benchmark])
    if benchmark == 'sotr':
        fix_sotr_prefs()

def ldconfig(build):
    print(f"loading built mesa as default driver: {build}")
    run(["sudo", "ldconfig",
         f"/home/perforate/mesa/{build}/build_64/install/lib/x86_64-linux-gnu",
         f"/home/perforate/mesa/{build}/build_64/install/lib/x86_64-linux-gnu/dri",
         f"/home/perforate/mesa/{build}/build_32/install/lib",
         f"/home/perforate/mesa/{build}/build_32/install/lib/dri"])

def make_steam_links():
    run(["/usr/local/bin/make_steam_links.sh"])

def edit_vdf(key, value, vdf_file):
    assert os.path.exists(vdf_file)
    run(["/usr/local/bin/vdf_tool.py", "--key", key, "--value", str(value), vdf_file])

def launch_steam(build, app_id, env):
    steam_expiration = int(time.time()) + 360000
    steam_dir = '/home/perforate/.local/share/Steam'
    if not os.path.exists(steam_dir):
        steam_dir = '/home/perforate/.steam/debian-installation'
    edit_vdf('/ClientInfo/valid_until', steam_expiration, f"{steam_dir}/update_hosts_cached.vdf")
    edit_vdf(f'UserLocalConfigStore/Software/Valve/Steam/apps/{app_id}/LaunchOptions',
             f"/usr/local/bin/with-mesa {build} %command%",
             f"{steam_dir}/userdata/117774964/config/localconfig.vdf")

    return run(["/usr/local/bin/with-mesa", build, "steam", "-noverifyfiles"],
               env=env, background=True)

def quit_steam():
    print("Stopping steam")
    run(["steam", "-shutdown"])
    while run(["pgrep", "^steam$"], check=False).returncode == 0:
        print("waiting for steam to exit")
        time.sleep(4)

def update_device_id(app):
    userdata = "/home/perforate/.steam/debian-installation/userdata/117774964"
    steam_cfg = {'dota' : f'{userdata}/570/local/cfg/video.txt'}
    if app not in steam_cfg:
        return
    card_file = get_card_file()
    with open(f"{card_file}/device/vendor", encoding="utf-8") as vendor:
        vendor_id = int(vendor.readline(), 16)
    with open(f"{card_file}/device/device", encoding="utf-8") as device:
        device_id = int(device.readline(), 16)
    edit_vdf("video.cfg/VendorID", vendor_id, steam_cfg[app])
    edit_vdf("video.cfg/DeviceID", device_id, steam_cfg[app])

def run_benchmark(benchmark, before, target, after, iterations, display=None):
    steam_app_id = {'dota' : 570,
                    'sotr' :  750920,
                    }
    abnpy = "/abn/src/abn_cmd.py"
    tracipy = "/traci/tools/traci/traci.py"
    abn_cmd =  {'dota' : [abnpy, "-t", "Dota 2 (replay Sept 2023)", "-s", "vk-g2"],
                'sotr' :  [abnpy, "-t", "Shadow of the Tomb Raider", "-s", "vk-g2"],
                'superposition' : [abnpy, "-t", "UnigineSuperposition", "-s", "ogl-g2"],
                'valley' : [abnpy, "-t", "UnigineValley", "-s", "ogl-g2"],
                }
    for t_bench, trace_file in TRACI_BENCHMARKS.items():
        abn_cmd[t_bench] = [tracipy, 'run', trace_file]

    update_device_id(benchmark)
    to_run = {'before' : before,
              'target' : target,
              'after' : after}

    score_data = Scores(repo="/home/perforate/mesa")
    score_data.read()
    score_data.clear_scores()

    save_dir = os.getcwd()
    os.chdir("/home/perforate")

    for build, commit in to_run.items():
        if not commit:
            continue
        if commit == "main":
            # resolve to a sha1
            commit = git.Repo("/home/perforate/mesa").commit("main").hexsha

        install_root = f"/home/perforate/mesa/{build}/build_64/install"
        if not os.path.exists(install_root):
            # build_failed
            score_data.add_score(benchmark, commit, 0, freq=0)
            continue

        env = {}
        if display == "headless":
            env["DISPLAY"] = start_headless(build)
        elif display == "xorg":
            if not start_x(build):
                print(f"ERROR: failing measurement for {commit}")
                score_data.add_score(benchmark, commit, 0, freq=0)
                continue

        steam_proc = None
        if benchmark in steam_app_id:
            steam_proc = launch_steam(build, steam_app_id[benchmark], env)
            time.sleep(5)

        cmd = ["/usr/local/bin/with-mesa", build] + abn_cmd[benchmark]
        if benchmark in ['dota']:
            # warmup required
            print(f"INFO: Running warmup iteration for {commit}")
            run(cmd, env=env)

        gpu_freq_limit = Scores(
            score_host="mesa-ci-perforate.local"
        ).get_gpu_frequency_limit(os.environ.get("NODE_NAME"))
        if gpu_freq_limit:
            limit_gpu_frequency(int(gpu_freq_limit))
        else:
            # must be running valley to determine the limit
            assert benchmark == "valley"
            gpu_freq_limit = 0

        for i in range(iterations):
            for result in glob.glob("results/json/*/*json"):
                os.unlink(result)
            for result in glob.glob("/traci/tools/traci/out/*csv"):
                os.unlink(result)
            print(f"Running iteration {i+1} for {commit}")
            freq_monitor = ThreadedFreqMonitor(interval=1)
            freq_monitor.start()
            run(cmd, env=env)
            freq_monitor.stop()
            freq_readings = freq_monitor.frequency_profile(gpu_freq_limit)
            avg_freq = 0
            if freq_readings:
                avg_freq = sum(freq_readings) / len(freq_readings)
            for result in glob.glob("results/json/*/*json"):
                a_score = json.load(open(result, encoding="utf8"))
                for scalar in a_score["result"]["scalars"]:
                    if scalar["metricName"] == "Avg FPS":
                        score_data.add_score(benchmark, commit,
                                             scalar["value"],
                                             freq=avg_freq)
                        print(f"Score {i+1}: {scalar['value']}")
                        break

            traci_result = "/traci/tools/traci/out/results.csv"
            if os.path.exists(traci_result):
                with open(traci_result, encoding='utf8') as csvfile:
                    reader = csv.DictReader(csvfile, skipinitialspace=True)
                    for row in reader:
                        if "TIMEOUT" in row["fps"]:
                            row["fps"] = 0
                        score_data.add_score(benchmark, commit,
                                             row["fps"],
                                             freq=avg_freq)

        limit_gpu_frequency("default")

        if steam_proc:
            quit_steam()
        if display == "xorg":
            stop_x()
    os.chdir(save_dir)
    score_data.write()

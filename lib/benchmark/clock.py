import collections
import math
import os
import threading
import time
from .command import run


def get_card_file() -> str:
    """Determine the sysfs entry for first gpu card on the system

    Returns: path to /sys/class/drm/card{n}
    """
    for i in range(4):
        if os.path.exists(f"/sys/class/drm/card{i}"):
            return f"/sys/class/drm/card{i}"
    assert False


def limit_gpu_frequency(rate : int | str) -> None:
    """set the maximum gpu frequency to the provided value

    Parameters:
      rate:

         integer value sets the minimum and maximum limit to provided
         clock frequency

         'default' string sets the minimum and maximum to the
         lowest/highest supported on the platform
    """
    card_file = get_card_file()
    if isinstance(rate, int):
        control_files = collections.deque(["gt_boost_freq_mhz",
                                           "gt_max_freq_mhz",
                                           "gt_min_freq_mhz"])
        while control_files:
            # the order of changes can cause some manipulations to
            # fail.  For example, if the min is set higher than the
            # current max.  Retry all changes in order until the last one completes.
            cur = control_files.pop()
            proc = run(["sudo", "/usr/local/bin/set_gpu_freq.sh",
                        f"{card_file}/{cur}", str(rate)],
                       check=False)
            if proc.returncode:
                control_files.appendleft(cur)

        # when controlling the gpu frequency, disable rc6.  CPU
        # intensive workloads like trace replay can cause the gpu to
        # sleep, inspite of a fixed frequency
        run(["sudo", "/usr/local/bin/set_rc6.sh", "off"])
        return rate

    if rate != "default":
        print("ERROR: invalid frequency: {rate}")
        return None

    # increase max by 50MHz until it can be lowered no more
    # lower min by 50MHz until it can be lowered no more
    for freq in ["gt_boost_freq_mhz", "gt_max_freq_mhz"]:
        cur = None
        with open(f'{card_file}/{freq}', encoding='utf8') as fh:
            cur = int(fh.readline())
        while True:
            cur = cur + 50
            proc = run(["sudo", "/usr/local/bin/set_gpu_freq.sh",
                        f"{card_file}/{freq}", str(cur)],
                       check=False)
            if proc.returncode:
                break

    cur = None
    with open(f'{card_file}/gt_min_freq_mhz', encoding='utf8') as fh:
        cur = int(fh.readline())
    while True:
        cur = cur - 50
        proc = run(["sudo", "/usr/local/bin/set_gpu_freq.sh",
                    f"{card_file}/gt_min_freq_mhz", str(cur)],
                   check=False)
        if proc.returncode:
            break

    # return the rc6 sleep configuration to default operation
    run(["sudo", "/usr/local/bin/set_rc6.sh", "on"])
    return None


class ThreadedFreqMonitor(threading.Thread):
    def __init__(self, interval : int):
        threading.Thread.__init__(self, name="gpu frequency monitor")
        self._interval = interval
        self._stop_flag = False
        self._freqs = []
        self._act_fh = open(f"{get_card_file()}/gt_act_freq_mhz", 'r', encoding="utf8")

    def stop(self):
        self._stop_flag = True
        self.join()
        self._act_fh.close()

    def run(self):
        while not self._stop_flag:
            self._act_fh.seek(0)
            self._freqs.append(int(self._act_fh.readline().strip()))
            time.sleep(self._interval)

    def max_stable_freq(self):
        freqs = self._freqs.copy()
        num_values_10s = 10 * int(1/self._interval)
        # discard the final 10s, which may include execution where gpu was not busy
        freqs = freqs[:-1 * num_values_10s]

        # discard the min, which will be the idle clock rate
        idle = min(freqs)
        drop_min_value = idle
        while True:
            freqs = [freq for freq in freqs if freq > drop_min_value]
            drop_min_value = min(freqs)
            # stop if there is more than 10s at the minimum value
            if freqs.count(drop_min_value) > num_values_10s:
                break

        tdp_limit = min(freqs)
        stable_freq = math.floor(tdp_limit * .9 / 50) * 50
        return stable_freq

    def frequency_profile(self, min_limit):
        """return the frequency measurements taken during the
        duration.  min_limit indicates the gpu frequency limit that
        was set during the collection.  Discard clock readings that
        are less than half of the limit, because they indicate that
        the gpu was idle.
        """
        return [freq for freq in self._freqs if freq > min_limit/2]

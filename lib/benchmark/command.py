import os
import subprocess

COMMAND_LOG = None

def run(command, check=True, capture_output=False, background=False, verbose=False,
        input=None, env=None):
    """wrap subprocess to allow verbosity and capture of command stream"""
    global COMMAND_LOG
    outflag = subprocess.DEVNULL
    if verbose:
        outflag = None
    if COMMAND_LOG:
        outflag = None
        amp = ""
        out_str = ""
        if env:
            out_str = " ".join([f"{k}={v}" for k,v in env.items()]) + " "
        for param in command:
            if " " in param:
                out_str += f'"{param}" '
            else:
                out_str += f"{param} "
        if background:
            out_str += "&"
        print(out_str)
        print(out_str, file=COMMAND_LOG)
        COMMAND_LOG.flush()
    if env:
        # enhance env with existing system settings
        for k, v in os.environ.items():
            if k not in env:
                env[k]=v
    else:
        # caller may pass {}, but the default to subprocess should be None
        env = None
    if background:
        assert not capture_output
        assert not input
        return subprocess.Popen(command,
                                stdout=outflag,
                                stderr=outflag,
                                env=env)
    if capture_output:
        # caller is processing the output, so the command must succeed
        assert check
        outflag = None
    text = None
    if input:
        text = True
    return subprocess.run(command,
                          capture_output=capture_output,
                          input=input,
                          text=text,
                          stdout=outflag,
                          stderr=outflag,
                          check=check,
                          env=env)

def capture_commands(path):
    """enable capture of executed commands in a file, for replay/debug"""
    global COMMAND_LOG
    COMMAND_LOG = open(path, "a", encoding="utf8")

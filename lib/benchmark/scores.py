import collections
import json
import os
import socket
import time

import git
import MySQLdb

from .command import run

tree = lambda: collections.defaultdict(tree)

class Scores:
    def __init__(self, score_host=None, repo=None):
        self._host = socket.gethostname()
        self._kernel = run(['uname', '-a'], capture_output=True).stdout.decode("utf8").strip()
        self._benchmark = tree()
        self._db = None
        self._score_host = score_host
        self.cur = None
        self._repo = None
        if repo:
            self._repo = git.Repo(repo)

    def _connect(self):
        if self._db:
            return
        self._db = MySQLdb.connect(host=self._score_host,
                                   user=os.environ.get("SQL_DATABASE_USER"),
                                   password=os.environ.get("SQL_DATABASE_PW"),
                                   use_unicode=True,
                                   charset="utf8",
                                   ssl={}) # disable ssl
        self._db.set_character_set('utf8')
        self.cur = self._db.cursor()
        self.cur.execute('SET NAMES utf8;')
        self.cur.execute('SET CHARACTER SET utf8;')
        self.cur.execute('SET character_set_connection=utf8;')
        self.cur.execute('use scores;')

    def query(self, benchmark, commits):
        if self._score_host:
            self._connect()
        benchmark_data = self._benchmark[benchmark]
        for commit in commits:
            if not commit:
                continue
            if commit not in benchmark_data and self._repo:
                git_commit = self._repo.commit(commit)
                benchmark_data[commit] = {
                    "date" : git_commit.committed_date,
                    "author" : str(git_commit.author),
                    "summary" : git_commit.summary,
                    "scores" : []}
            else:
                benchmark_data[commit] = {"scores" : []}
            score_data = benchmark_data[commit]["scores"]
            scores = self._get_fields(["sha", "date", "author", "summary",
                                       "benchmark_name", "fps", "freq"],
                                      {"host_name" : self._host,
                                       "benchmark_name" : benchmark,
                                       "sha" : commit})
            for score in scores:
                if "author" not in benchmark_data[commit]:
                    commit_data = benchmark_data[commit]
                    commit_data["author"] = score["author"]
                    commit_data["summary"] = score["summary"]
                    commit_data["date"] = score["date"]
                score_data.append(
                    {"fps" : score["fps"],
                     "freq" : score["freq"]} )

    def write(self, scorefile = "/scores/scores.json"):
        data = tree()
        data["host"] = self._host
        data["kernel"] = self._kernel
        data["benchmark"] = self._benchmark
        with open(scorefile, "w", encoding="utf8") as fh:
            json.dump(data, fh)

    def read(self):
        score_file = "/var/results/old_scores.json"
        if not os.path.exists(score_file):
            return
        with open(score_file, encoding="utf8") as fh:
            data = json.load(fh)
            self._host = data["host"]
            self._kernel = data["kernel"]
            self._benchmark = data["benchmark"]

    def clear_scores(self):
        for commits in self._benchmark.values():
            for commit in commits.values():
                commit["scores"] = []

    def add_score(self, benchmark, commit, score, freq):
        git_commit = self._repo.commit(commit)
        if benchmark not in self._benchmark:
            # benchmark will not be pre-loaded to get scores if we are
            # running valley to obtain gpu freq at tdp
            return
        benchmark = self._benchmark[benchmark]
        if commit not in benchmark:
            benchmark[commit] = {"scores" : []}
        commit_data = benchmark[commit]
        if "date" not in commit_data:
            commit_data["date"] = git_commit.committed_date
            commit_data["author"] = str(git_commit.author)
            commit_data["summary"] = git_commit.summary

        benchmark[commit]["scores"].append({"fps":score, "freq":freq})

    def _get_fields(self, fields, where):
        if not self._score_host:
            return []
        where_clause = ""
        if where:
            where_clause = " where " + " and ".join([f"{k}=%({v})s" for k,v in where.items()])
        where_dict = { v : v  for _, v in where.items() }
        qstr = (f"select {', '.join(fields)} from score "
                "join host using (host_id) "
                "join commit using (commit_id) "
                "join benchmark using (benchmark_id) "
                + where_clause)
        self.cur.execute(qstr, where_dict)
        results = []
        for c in self.cur.fetchall():
            results.append(dict(zip(fields, c)))
        return results

    def longest_gap(self, benchmark):
        """returns the 2 git commits with the longest gap with no datapoints"""
        self._connect()
        self.cur.execute("use scores")
        scores = self._get_fields(["sha", "date", "benchmark_name", "host_name"],
                                 where={"benchmark_name": benchmark,
                                        "host_name" : self._host})
        commits = {score['date'] : score['sha'] for score in scores}
        dates = sorted(list(commits.keys()))
        if len(dates) < 2:
            # provide default start/end points
            return ("b9c9fb7259a37c818e11fcc8de9401cc7f4847b2",
                    "c6e855b64b9015235462959b2b7f3e9fc34b2f1f")
        age_of_newest_commit_hrs = (time.time() - dates[-1]) / (60 * 60)
        if age_of_newest_commit_hrs > 24:
            # build main
            return ("main", "main")

        max_index = 1
        max_delta = dates[1] - dates[0]
        index = 2
        while index < len(dates):
            cur_delta =  dates[index] - dates[index-1]
            if cur_delta > max_delta:
                max_delta = cur_delta
                max_index = index
            index += 1
        return (commits[dates[max_index-1]], commits[dates[max_index]])

    def choose_benchmark(self, host, benchmarks):
        """determine which benchmark has the longest gap in scores,
        for the given host"""
        self._connect()
        self.cur.execute("use scores")
        scores = self._get_fields(["date", "benchmark_name", "host_name"],
                                 where={"host_name" : host})
        benchmark_dates = { benchmark : [] for benchmark in benchmarks }
        for score in scores:
            benchmark = score['benchmark_name']
            date = score['date']
            if benchmark not in benchmarks:
                # ignore result lines for benchmarks that are not
                # expected to be on the machine.
                continue
            benchmark_dates[benchmark].append(date)

        benchmark_max_delta = { benchmark: 0 for benchmark in benchmarks }
        for benchmark in benchmarks:
            dates = sorted(benchmark_dates[benchmark])
            if len(dates) < 2:
                # start the first build of the benchmark
                return benchmark
            age_of_newest_commit_hrs = (time.time() - dates[-1]) / (60 * 60)
            if age_of_newest_commit_hrs > 24:
                # start a daily build of the benchmark
                return benchmark

            index = 1
            while index < len(dates):
                cur_delta =  dates[index] - dates[index - 1]
                if cur_delta > benchmark_max_delta[benchmark]:
                    benchmark_max_delta[benchmark] = cur_delta
                index += 1
        return max(benchmark_max_delta, key=benchmark_max_delta.get)

    def get_gpu_frequency_limit(self, perf_host):
        self._connect()
        self.cur.execute("select freq_limit from frequencylimit where "
                         "host_name=%(perf_host)s",
                         {"perf_host": perf_host})
        ret = self.cur.fetchone()

        return ret[0] if ret else ret

    def set_gpu_frequency_limit(self, perf_host, freq_limit):
        self._connect()
        self.cur.execute("insert into frequencylimit values "
                         "(%(perf_host)s, %(freq_limit)s) "
                         "on duplicate key update freq_limit=%(freq_limit)s",
                         {"perf_host": perf_host,
                          "freq_limit": freq_limit})
        self._db.commit()

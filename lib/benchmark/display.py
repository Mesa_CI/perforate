import os
import tempfile
import time
from benchmark import run

HEADLESS_WESTON_PID = None
XORG_PROC = None
OPENBOX_PROC = None

def start_headless(build=None):
    global HEADLESS_WESTON_PID
    xdg_dir = tempfile.TemporaryDirectory()
    log_dir = tempfile.TemporaryDirectory()
    weston_log = f"{log_dir.name}/weston.log"
    # create empty xdg dir under /run
    env = {"XDG_RUNTIME_DIR" : xdg_dir.name}
    # launch weston
    cmd = []
    if build:
        cmd = ["/scripts/with-mesa", build]
    cmd += ["weston", "-Bheadless", "--xwayland", "-i0",
            "--width=1920", "--height=1080", "--renderer=gl",
            "--log", weston_log]
    HEADLESS_WESTON_PID = run(cmd, env=env, background=True).pid
    time.sleep(2)

    # process output for DISPLAY
    logfh = open(weston_log, 'r', encoding="utf8")
    for aline in logfh.readlines():
        if "xserver listening on display" in aline:
            display = aline.split()[-1]
            print(f"INFO: started weston headless on {display}")
            return display
    # else
    print("ERROR: could not start weston")
    stop_headless()

def stop_headless():
    global HEADLESS_WESTON_PID
    assert HEADLESS_WESTON_PID
    run(["kill", "SIGINT", str(HEADLESS_WESTON_PID)], check=False)

def start_x(build=None):
    global XORG_PROC, OPENBOX_PROC
    if build:
        bpath = f"/home/perforate/mesa/{build}"
        run(["sudo", "ldconfig",
             f"{bpath}/build_64/install/lib/x86_64-linux-gnu",
             f"{bpath}/build_64/install/lib",
             f"{bpath}/build_64/install/lib/i386-linux-gnu",
             f"{bpath}/build_32/install/lib/x86_64-linux-gnu",
             f"{bpath}/build_32/install/lib",
             f"{bpath}/build_32/install/lib/i386-linux-gnu"])
    run(["sudo", "killall", "Xorg"], check=False, verbose=False)
    run(["sudo", "rm", "/tmp/.X0-lock"], check=False, verbose=False)
    time.sleep(5)
    XORG_PROC = run(["sudo", "Xorg", ":0", "vt0", "-retro", "-sharevts", "-noreset"],
                    background=True)
    time.sleep(5)
    try:
        OPENBOX_PROC = run(["sudo", "openbox"],
                           background=True, verbose=False)
        run(["sudo", "xhost", "+"])
    except:
        print("ERROR: unable to start X")
        stop_x()
        return False
    return True

def stop_x():
    global XORG_PROC, OPENBOX_PROC
    for proc in XORG_PROC, OPENBOX_PROC:
        assert proc
        proc.kill()
        run(["sudo", "kill", str(proc.pid)], check=False, verbose=False)
    time.sleep(5)
    OPENBOX_PROC = None
    XORG_PROC = None

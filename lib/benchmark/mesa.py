import os
import socket
from benchmark import run

REPO_MIRRORS = [
    "git://otc-mesa-ci.local/git/mirror/mesa/origin",
    "git://otc-mesa-ci.local/git/mirror/mesa/ci_mesa_repo",
    "git://otc-mesa-ci.jf.intel.com/git/mirror/mesa/origin",
    "git://otc-mesa-ci.jf.intel.com/git/mirror/mesa/ci_mesa_repo",
    "https://gitlab.freedesktop.org/mesa/mesa.git",
    "https://gitlab.freedesktop.org/Mesa_CI/repos/mesa.git",
]

UNREACHABLE_HOSTS = []

def _host_from_mirror(url):
    return url.split('/')[2]

def _find_accessible_mirror(src_dir=None, commit=None):
    global UNREACHABLE_HOSTS
    for url in REPO_MIRRORS:
        host = _host_from_mirror(url)
        if host in UNREACHABLE_HOSTS:
            continue
        try:
            socket.gethostbyname(host)
        except:
            UNREACHABLE_HOSTS.append(host)
            continue
        if not commit:
            return url
        # else check that the remote has the target commit
        result = run(["git", "-C", src_dir,
                      "fetch", url, commit,
                      "-q", "--dry-run"])
        if result.returncode == 0:
            return url
    if not commit:
        print("Error: no mesa repositories are accessible from this system")
    else:
        print(f"Error: no mesa accessible repositories contain {commit}")
    assert False

def clone(src_dir):
    if os.path.exists(f"{src_dir}/.git"):
        return
    origin = _find_accessible_mirror()
    print(f"Cloning mesa repo from {origin}")
    if os.stat(src_dir).st_uid == 0:
        # newly created volume is root-owned
        run(["sudo", "chown", "-R", "perforate", src_dir])
        run(["sudo", "chgrp", "-R", "perforate", src_dir])
    run(["git", "-C", src_dir, "clone", "--no-tags", origin, src_dir])

def clean(src_dir):
    if os.path.exists(f"{src_dir}/.git/index.lock"):
        run(["rm", "-rf", f"{src_dir}/.git/index.lock"])
    run(["git", "-C", src_dir, "clean", "-xfd"])
    run(["rm", "-rf", f"{src_dir}/.git/rebase-apply"])

def fetch(src_dir, commit):
    # TODO: handle tags as well as naked commits
    print(f"INFO: fetching {commit}")
    run(["git", "-C", src_dir,
         "fetch",
         "-f", "--no-tags", "--update-head-ok",
         _find_accessible_mirror(src_dir, commit), commit])
    if commit == "main":
        # force main to be the recently fetched main
        run(["git", "-C", src_dir, "checkout", "--detach", "main"])
        run(["git", "-C", src_dir, "branch", "-f",
             "main", "FETCH_HEAD"])
    result = run(["git", "-C", src_dir,
                  "cat-file", "-t", commit], check=False)
    if result.returncode != 0:
        print("Error: could not find {commit} in repos")
        assert False

def checkout(src_dir, commit):
    run(["git", "-C", src_dir, "checkout", "--detach", commit])
    run(["git", "-C", src_dir, "reset", "--hard", "HEAD"])

def build(src_dir, target):
    print(f"INFO: building mesa for {target}")
    run(["meson", "setup",
         "--prefix", f"{src_dir}/{target}/build_64/install",
         "-Dbuildtype=release", "-Dvulkan-drivers=intel",
         "-Dplatforms=x11", "-Dgallium-drivers=swrast,iris",
         "-Dllvm=enabled", "-Dtools=intel",
         f"{src_dir}/{target}/build_64", src_dir
         ])
    result = run(["ninja", "-C", f"{src_dir}/{target}/build_64", "install"],
                 check=False)
    if result.returncode:
        print(f"ERROR: compilation error for {target}")

def bisect_commit(src_dir, before, after):
    print(f"INFO: finding target commit by bisecting {before}..{after}")
    return run(["git", "-C", src_dir,
                "rev-list", "--bisect",
                f"{before}..{after}"],
               capture_output=True).stdout.decode("utf8").strip()

#!/bin/bash

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

if [ ! -f /etc/systemd/system/weston.service ]; then
    cp ${SCRIPT_DIR}/weston.service /etc/systemd/system/
    systemctl daemon-reload
fi

systemctl stop gdm
systemctl start weston

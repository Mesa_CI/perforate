#!/bin/bash

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

`which docker-compose > /dev/null 2>&1`
NEEDS_INSTALL=$?
if [ -e /etc/docker/daemon.json ] && [ !$NEEDS_INSTALL ]; then
   echo "no action needed"
   exit
fi

if [[ "$EUID" != 0 ]]; then
    sudo -k # make sure to ask for password on next sudo
    if sudo true; then
	echo "setting up docker-compose for perforate"
    else
        echo "wrong password"
        exit 1
    fi
fi

if [ $NEEDS_INSTALL ]; then
   sudo apt install docker-compose
fi

if [ ! -e /etc/docker/daemon.json ]; then
    sudo cp $SCRIPT_DIR/daemon.json /etc/docker/
    sudo systemctl restart docker
fi

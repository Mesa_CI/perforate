;;; package --- my defaults
;;; Commentary:
;;; code:

(setq native-comp-async-jobs-number 8)
(setq native-comp-speed -1)
(setq package-native-compile 1)
(setq package-enable-at-startup nil)
(setq inhibit-startup-message t)
(setq custom-file "~/.emacs.d/custom.el")
(load custom-file 'noerror)
(require 'font-lock)  ; Decorate source files with fonts/color based on syntax
(require 'cc-mode)
(define-key isearch-mode-map "\C-h" 'isearch-delete-char)
(global-set-key "\C-h" 'backward-delete-char)
(global-set-key "\M-\C-h" 'backward-kill-word)
(global-set-key "\M-p" 'dabbrev-expand)
(global-set-key "\C-cC" 'uncomment-region)
(global-set-key "\C-c\C-i" 'magit-status)
(global-set-key "\C-ci" 'magit-status)
(global-set-key "\C-cf" 'auto-fill-mode)
(global-set-key [(control o)]  'other-window)
(global-set-key [(control meta o)]  "\C-u-\C-xo")
(global-set-key "\C-cb" 'bury-buffer)
;; for some reason there is a big delay here
(require 'ido)
(ido-mode t)
(define-key ido-file-dir-completion-map "\C-h" 'ido-delete-backward-updir)
(add-hook 'isearch-mode-end-hook 'my-goto-match-beginning)
(defun my-goto-match-beginning ()
  (when (and isearch-forward isearch-other-end)
    (goto-char isearch-other-end)))


; disable the mail shortcut, which is annoying
(global-set-key "\C-xm" 'wl)

; this setting makes lines wrap when viewing 2 vertical windows
(setq truncate-partial-width-windows nil)

(if (display-graphic-p)
    ; does not work in a terminal window
    (progn
      (global-set-key "\M-]" 'forward-paragraph)
      (global-set-key "\M-[" 'backward-paragraph)
      )
  )

;; DIRED
(add-hook 'dired-load-hook
	  '(lambda()
	     (define-key dired-mode-map
	       [(control o)]  'other-window)
	     (define-key dired-mode-map
	       [(meta s)]  'swiper)
	     (load "dired-x")))

(require 'compile)
(define-key compilation-mode-map [(control o)]  'other-window)

; when you are searching and replacing, attempt to preserve case in substitution
(setq case-replace nil)

;; this variable ignores case when tab-completing in find-file (c-x c-f)
(setq completion-ignore-case t)

(global-font-lock-mode t)

; allow m-# to call up calc
(global-set-key "\e#" 'calc-dispatch)

; allow upcase/downcase commands
(put 'downcase-region 'disabled nil)
(put 'upcase-region 'disabled nil)

(display-time)

(put 'set-goal-column 'disabled nil)
(put 'narrow-to-region 'disabled nil)

(add-to-list 'auto-mode-alist '("\\.org\\'" . org-mode))

; TODO
;(require 'cmake-project)
(if (file-readable-p (expand-file-name "~/.emacs.d/myinit.org"))
  (org-babel-load-file (expand-file-name "~/.emacs.d/myinit.org")))

(put 'dired-find-alternate-file 'disabled nil)

(require 'org-crypt)

(native-compile-async "/usr/share/emacs/29.1/lisp" 'recursively)
(while (or comp-files-queue
           (> (comp-async-runnings) 0))
  (sleep-for 1))

;;; .emacs ends here

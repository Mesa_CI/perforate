#!/usr/bin/env python3

import glob
import json
import os
import sys
import time

import inotify.adapters
import MySQLdb

RESULTS_PATH = '/scores'

class Importer:
    """imports scores when new files are sent to the server
    """
    def __init__(self):
        self.inot = inotify.adapters.Inotify()
        self.inot.add_watch(RESULTS_PATH)
        with open("/run/secrets/mysql-pw-perforate", 'r', encoding="utf8") as pw_fh:
            self.sql_pw = pw_fh.read().rstrip()
        self._connect()

    def _connect(self):
        connected = False
        while not connected:
            try:
                self.db = MySQLdb.connect(host="sql", user="perforate",
                                              passwd=self.sql_pw,
                                              use_unicode=True, charset="utf8mb4")
                connected = True
            except MySQLdb._exceptions.OperationalError:
                print("WARN: Unable to connect to sql server, retrying in 5 seconds...")
                time.sleep(5)
        self.db.set_character_set('utf8mb4')
        self.cur = self.db.cursor()
        self.cur.execute('SET NAMES utf8mb4;')
        self.cur.execute('SET CHARACTER SET utf8mb4;')
        self.cur.execute('SET character_set_connection=utf8mb4;')
        self._create_db()

    def _create_db(self):
        """If the target database does not exist, then create it and
        initialize all tables."""
        # '-' and '.' not allowed in database names
        self.cur.execute("create database if not exists scores")
        self.cur.execute("use scores")
        self.cur.execute("create table if not exists host ("
                         "host_id int not null AUTO_INCREMENT, "
                         "host_name char(100), "
                         "kernel char(200), "
                         "primary key(host_id), "
                         "index host_name(host_name))")

        self.cur.execute("create table if not exists benchmark ("
                         "benchmark_id int not null AUTO_INCREMENT, "
                         "benchmark_name char(100), "
                         "primary key(benchmark_id), "
                         "index benchmark_name(benchmark_name))")

        self.cur.execute("create table if not exists commit ("
                         "commit_id int not null AUTO_INCREMENT, "
                         "sha char(64), "
                         "date int not null, "
                         "author char(100) not null, "
                         "summary char(200) not null, "
                         "primary key(commit_id), "
                         "index date(date))")

        self.cur.execute("create table if not exists score ("
                         "score_id int not null AUTO_INCREMENT, "
                         "host_id int not null, "
                         "commit_id int not null, "
                         "benchmark_id int not null, "
                         "fps float not null, "
                         "freq float not null, "
                         "primary key(score_id), "
                         "index (host_id, benchmark_id, commit_id))")

        self.cur.execute("create table if not exists frequencylimit("
                         "host_name char(100), "
                         "freq_limit float not null, "
                         "primary key(host_name), "
                         "index (host_name))")

        self.db.commit()


    def poll(self):
        """ Blocks until a new score is found by inotify, then
        returns the path when one is found """
        for event in self.inot.event_gen(yield_nones=False):
            (_, type_names, path, filename) = event
            new_file = path + "/" + filename
            if not new_file.endswith(".json"):
                continue
            # rsync will upload to a temporary, then move to the final
            # filename.  Only proceed if the event was an mv
            if 'IN_MOVED_TO' not in type_names:
                continue
            if not new_file or not os.path.exists(new_file):
                continue
            return new_file

    def values_and_fields(self, row_dict):
        """format a string to insert or update a row"""
        columnstring = []
        paramstring = []
        for k in row_dict.keys():
            columnstring.append(k)
            paramstring.append(f"%({k})s")
        paramstring = ", ".join(paramstring)
        columnstring = ", ".join(columnstring)
        return f"({columnstring}) values({paramstring})"

    def import_score(self, scorefile):
        with open(scorefile, encoding="utf8") as fh:
            data = json.load(fh)
        host = data['host']
        sqlstr = f'select count(*), host_id, kernel from host where host_name="{host}"'
        self.cur.execute(sqlstr)
        (count, host_id, kernel) = self.cur.fetchone()
        if count == 0:
            kernel = data['kernel']
            self.cur.execute(f'insert into host(host_name, kernel) values("{host}", "{kernel}")')
            self.db.commit()
            self.cur.execute("select last_insert_id()")
            host_id = self.cur.fetchone()[0]
        for benchmark, commits in data["benchmark"].items():
            sqlstr = ('select count(*), benchmark_id from benchmark '
                      f'where benchmark_name="{benchmark}"')
            self.cur.execute(sqlstr)
            (count, benchmark_id) = self.cur.fetchone()
            if count == 0:
                self.cur.execute(f'insert into benchmark(benchmark_name) values("{benchmark}")')
                self.db.commit()
                self.cur.execute("select last_insert_id()")
                benchmark_id = self.cur.fetchone()[0]
            for commit, commit_data in commits.items():
                # "main" is sent as a commit to trigger a test of
                # latest mesa.  It is update at build/exection time to
                # the lastes commit sha, but an empty "main" entry is
                # tedious to remove from the scores file.
                if commit == "main":
                    continue
                self.cur.execute(f'select count(*), commit_id from commit where sha="{commit}"')
                (count, commit_id) = self.cur.fetchone()
                if count == 0:
                    values = { "sha" : commit,
                               "date" : commit_data["date"],
                               "author" : commit_data["author"],
                               "summary" : commit_data["summary"],
                              }
                    insert_str = self.values_and_fields(values)
                    self.cur.execute(f'insert into commit {insert_str}', values)
                    self.db.commit()
                    self.cur.execute("select last_insert_id()")
                    commit_id = self.cur.fetchone()[0]
                for score in commit_data["scores"]:
                    values = {"host_id" : host_id,
                              "benchmark_id" : benchmark_id,
                              "commit_id" : commit_id,
                              "fps" : score["fps"],
                              "freq" : score["freq"]}
                    insert_str = self.values_and_fields(values)
                    self.cur.execute(f'insert into score {insert_str}', values)
        self.db.commit()

        print(f"removing {scorefile}")
        os.unlink(scorefile)

def main():
    """main routine"""
    assert os.path.exists(RESULTS_PATH)

    importer = Importer()
    if len(sys.argv) > 1:
        for score in sys.argv[1:]:
            importer.import_score(score)
        return

    # consume any existing tar files
    for score in glob.glob(f"{RESULTS_PATH}/*.json"):
        importer.import_score(score)
    importer = Importer()
    while True:
        score = importer.poll()
        importer.import_score(score)

if __name__ == "__main__":
    main()

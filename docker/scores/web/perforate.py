import datetime
import os
import statistics

import flask
from flask import g, request

import MySQLdb

app = flask.Flask(__name__)

def ServerError(f_request):
    prev_url = f_request.referrer or '/'
    return flask.render_template("500.html", url=prev_url), 500

@app.before_request
def before_request():
    """connects to the database and sets up a mysql cursor in g.cur"""
    sql_pw_file = "/run/secrets/mysql-pw-perforate"
    if os.path.exists(sql_pw_file):
        with open(sql_pw_file, 'r', encoding='utf8') as f:
            sql_pw = f.read().rstrip()
    try:
        g.db = MySQLdb.connect(host="sql", passwd=sql_pw, user="perforate",
                               use_unicode=True, charset="utf8")
    except MySQLdb._exceptions.OperationalError:
        print("ERROR: Unable to connect to sql server")
        return flask.ServerError(flask.request)

    g.db.set_character_set('utf8')
    g.cur = g.db.cursor()
    g.cur.execute('SET NAMES utf8;')
    g.cur.execute('SET CHARACTER SET utf8;')
    g.cur.execute('SET character_set_connection=utf8;')
    g.cur.execute("use scores")

@app.after_request
def after_request(response):
    """closes the session's database connection"""
    if g.get('db'):
        g.db.close()
    return response

def get_score_fields(cur, fields, where_clause=""):
    table = ("score join host using (host_id)"
             "join commit using (commit_id) "
             "join benchmark using (benchmark_id)")
    qstr = f"select {', '.join(fields)} from {table} {where_clause}"
    cur.execute(qstr)
    results = []
    for c in cur.fetchall():
        results.append(dict(zip(fields, c)))
    return results

def collate_scores(rows):
    # structure is data[hostname][benchmark][commit_date]["fps"|"freq"] = {avg: stdev:}
    #                                                    ["raw"] = [{fps: freq:}]
    scores = {}
    for score in rows:
        host_name = score['host_name']
        if host_name not in scores:
            scores[host_name] = {}
        host_data = scores[host_name]
        benchmark_name = score['benchmark_name']
        if benchmark_name not in host_data:
            host_data[benchmark_name] = {}
        bench_scores = host_data[benchmark_name]
        # python dates are in sec, java dates are in ms
        java_date = score["date"] * 1000
        if java_date not in bench_scores:
            bench_scores[java_date] = { "raw" : [] }
        bench_scores[java_date]["raw"].append({"fps" : score["fps"],
                                               "freq" : score["freq"]})
    for host_data in scores.values():
        for bench_scores in host_data.values():
            for score in bench_scores.values():
                raw_scores = score["raw"]
                for stat in ["fps", "freq"]:
                    series = [s[stat] for s in raw_scores]
                    if len(series) == 1:
                        score[stat] = {"avg" : series[0],
                                       "stdev" : 0.0}
                    else:
                        score[stat] = {"avg" : statistics.mean(series),
                                       "stdev" : statistics.stdev(series)}
    return scores

def collate_commits(rows):
    # structure is commits[date] = {date: author: summary: sha: }
    commits = {}
    for commit in rows:
        # python dates are in sec, java dates are in ms
        java_date = commit["date"] * 1000
        if java_date in commits:
            continue
        commits[java_date] = {"date" : datetime.datetime.fromtimestamp(commit["date"]).ctime(),
                              "author" : commit["author"],
                              "summary" : commit["summary"],
                              "sha" : commit["sha"],
                              }
    return commits

# the minimal Flask application
@app.route('/')
def index():
    """front page for results"""
    scores = get_score_fields(g.cur, ["sha", "date", "benchmark_name", "fps",
                                      "freq", "host_name", "kernel"])
    data = collate_scores(scores)
    trends = []
    for hostname, benchmark_data in sorted(data.items()):
        for benchmark_name, commit_data in benchmark_data.items():
            commit_times = sorted(commit_data.keys())
            newest_date = commit_times[-1]
            day_old_date = newest_date - (24 * 60 * 60 * 1000)
            week_old_date = newest_date - (7 * 24 * 60 * 60 * 1000)
            month_old_date = newest_date - (30 * 24 * 60 * 60 * 1000)
            quarter_old_date = newest_date - (90 * 24 * 60 * 60 * 1000)
            if day_old_date < commit_times[0]:
                day_old_date = newest_date
            if week_old_date < commit_times[0]:
                week_old_date = newest_date
            if month_old_date < commit_times[0]:
                month_old_date = newest_date
            if quarter_old_date < commit_times[0]:
                quarter_old_date = newest_date
            newest_score = commit_data[newest_date]["fps"]["avg"]
            day_old_score = None
            week_old_score = None
            month_old_score = None
            quarter_old_score = None
            for commit_time in reversed(commit_times):
                if quarter_old_score:
                    break
                if commit_time <= quarter_old_date:
                    quarter_old_score = commit_data[commit_time]["fps"]["avg"]
                if month_old_score:
                    continue
                if commit_time <= month_old_date:
                    month_old_score = commit_data[commit_time]["fps"]["avg"]
                if week_old_score:
                    continue
                if commit_time <= week_old_date:
                    week_old_score = commit_data[commit_time]["fps"]["avg"]
                if day_old_score:
                    continue
                if commit_time <= day_old_date:
                    day_old_score = commit_data[commit_time]["fps"]["avg"]
            daily_trend = 0
            weekly_trend = 0
            monthly_trend = 0
            quarterly_trend = 0
            if day_old_score:
                daily_trend = (newest_score - day_old_score) * 100 / day_old_score
            if week_old_score:
                weekly_trend = (newest_score - week_old_score) * 100 / week_old_score
            if month_old_score:
                monthly_trend = (newest_score - month_old_score) * 100 / month_old_score
            if quarter_old_score:
                quarterly_trend = (newest_score - quarter_old_score) * 100 / quarter_old_score
            trends.append({"host_name" : hostname,
                           "benchmark_name" : benchmark_name,
                           "daily" : daily_trend,
                           "weekly" : weekly_trend,
                           "monthly" : monthly_trend,
                           "quarterly" : quarterly_trend,
                           })
    return flask.render_template('main.html', trends=trends)

@app.route('/raw')
def raw():
    """raw table of results"""
    scores = get_score_fields(g.cur, ["sha", "date", "benchmark_name",
                                      "fps", "freq", "host_name", "kernel"],
                              "order by date")
    return flask.render_template('raw.html', scores=scores)

@app.route('/robots.txt')
def static_from_root():
    return flask.send_from_directory(app.static_folder, flask.request.path[1:])

def get_id(cur, field_name, name):
    query_str = f"select {field_name}_id from {field_name} where {field_name}_name like %(name)s"
    if field_name == "commit":
        query_str = f"select {field_name}_id from {field_name} where sha like %(name)s"
    cur.execute(query_str, {"name": name})
    try:
        _id = g.cur.fetchone()[0]
    except TypeError:
        return -1
    return _id

@app.route('/host/<hostname>')
def graph_for_host(hostname):
    g.cur.execute("select host_id from host where host_name like %(hostname)s",
                  {"hostname": hostname})
    host_id = get_id(g.cur, "host", hostname)
    if host_id < 0:
        return ServerError(request)
    scores = get_score_fields(g.cur, ["sha", "date", "summary", "author", "freq",
                                      "benchmark_name", "fps", "host_name", "kernel"],
                              f"where host_id={host_id}")
    data = collate_scores(scores)
    git_commits = collate_commits(scores)
    series = {}
    frequencies = {}
    stdev = {}
    for benchmark, commits in data[hostname].items():
        if benchmark not in series:
            series[benchmark] = []
            frequencies[benchmark] = []
            stdev[benchmark] = []
        for commit_date, score in sorted(commits.items()):
            series[benchmark].append([commit_date, score["fps"]["avg"]])
            frequencies[benchmark].append(f'{score["freq"]["avg"]:0.4g}')
            stdev[benchmark].append(f'{score["fps"]["stdev"]:0.4g}')
    return flask.render_template("flot.html",
                                 page_title=f"Scores for host {hostname}",
                                 page_name=hostname,
                                 data = {"points" : [{"label": benchmark, "data": scores}
                                                     for benchmark, scores in series.items()],
                                         "frequencies" : frequencies,
                                         "stdev" : stdev,
                                         "commits" : git_commits,
                                         })

@app.route('/benchmark/<benchmark>')
def graph_for_benchmark(benchmark):
    benchmark_id = get_id(g.cur, "benchmark", benchmark)
    if benchmark_id < 0:
        return ServerError(request)
    scores = get_score_fields(g.cur, ["sha", "date", "summary", "author", "freq",
                                      "benchmark_name", "fps", "host_name", "kernel"],
                              f"where benchmark_id={benchmark_id}")
    data = collate_scores(scores)
    git_commits = collate_commits(scores)
    series = {}
    frequencies = {}
    stdev = {}
    for host,_ in data.items():
        if benchmark not in series:
            series[host] = []
            frequencies[host] = []
            stdev[host] = []
        for benchmark_name, commits in data[host].items():
            assert benchmark_name == benchmark
            for commit_date, score in sorted(commits.items()):
                series[host].append([commit_date, score["fps"]["avg"]])
                frequencies[host].append(f'{score["freq"]["avg"]:0.4g}')
                stdev[host].append(f'{score["fps"]["stdev"]:0.4g}')
    return flask.render_template("flot.html",
                                 page_title=f"Scores for {benchmark}",
                                 page_name=benchmark,
                                 data = {"points" : [{"label": benchmark, "data": scores}
                                                     for benchmark, scores in series.items()],
                                         "frequencies" : frequencies,
                                         "stdev" : stdev,
                                         "commits" : git_commits,
                                         })

@app.route('/benchmark/<benchmark>/<hostname>/<sha>')
@app.route('/host/<hostname>/<benchmark>/<sha>')
def detail(hostname, benchmark, sha):

    benchmark_id = get_id(g.cur, "benchmark", benchmark)
    if benchmark_id < 0:
        return ServerError(request)
    host_id = get_id(g.cur, "host", hostname)
    if host_id < 0:
        return ServerError(request)
    commit_id = get_id(g.cur, "commit", sha)
    if commit_id < 0:
        return ServerError(request)
    where = f"where benchmark_id={benchmark_id} and host_id={host_id} and commit_id={commit_id}"
    scores = get_score_fields(g.cur, ["sha", "date", "summary", "author", "freq",
                                      "benchmark_name", "fps", "host_name", "kernel"], where)
    for s in scores:
        s["date_str"] = datetime.datetime.fromtimestamp(s["date"]).ctime()

    # find the previous / subsequent score
    where = f"where benchmark_id={benchmark_id} and host_id={host_id}"
    all_scores = get_score_fields(g.cur, ["sha", "date", "summary", "author", "freq",
                                          "benchmark_name", "fps", "host_name", "kernel"], where)
    commits_by_date = {score['date'] : score['sha'] for score in all_scores}
    commits_by_sha = {score['sha'] : score['date'] for score in all_scores}
    dates = sorted(list(commits_by_date.keys()))
    date_index = dates.index(commits_by_sha[sha])
    prev_score = {}
    next_score = {}
    build_link = "http://mesa-ci-jenkins.jf.intel.com/job/public/job/perforate/buildWithParameters?"
    bisect_params = { "benchmark" : benchmark,
                      "label" : hostname,
                      "BuildPriority" : "2",
                      }
    if date_index > 0:
        prev_sha = commits_by_date[dates[date_index - 1]]
        commit_id = get_id(g.cur, "commit", prev_sha)
        if commit_id < 0:
            return ServerError(request)
        where = f"where benchmark_id={benchmark_id} and host_id={host_id} and commit_id={commit_id}"
        prev_scores = get_score_fields(g.cur, ["sha", "date", "summary", "author", "freq",
                                               "benchmark_name", "fps", "host_name", "kernel"],
                                       where)
        prev_score["score"] = statistics.mean([s["fps"] for s in prev_scores])
        prev_score["stdev"] = 0
        if len(prev_scores) > 1:
            prev_score["stdev"] = statistics.stdev([s["fps"] for s in prev_scores])
        prev_score["freq"] = statistics.mean([s["freq"] for s in prev_scores])
        prev_score["sha"] = prev_sha
        bisect_params["before"] = prev_sha
        bisect_params["after"] = sha
        bisect_options = "&".join([f"{k}={v}" for k, v in bisect_params.items()])
        prev_score["bisect_link"] = f"{build_link}{bisect_options}"
    if date_index < len(dates) - 1:
        next_sha = commits_by_date[dates[date_index + 1]]
        commit_id = get_id(g.cur, "commit", next_sha)
        if commit_id < 0:
            return ServerError(request)
        where = f"where benchmark_id={benchmark_id} and host_id={host_id} and commit_id={commit_id}"
        next_scores = get_score_fields(g.cur, ["sha", "date", "summary", "author", "freq",
                                               "benchmark_name", "fps", "host_name", "kernel"],
                                       where)
        next_score["score"] = statistics.mean([s["fps"] for s in next_scores])
        prev_score["stdev"] = 0
        if len(next_scores) > 1:
            next_score["stdev"] = statistics.stdev([s["fps"] for s in next_scores])
        next_score["freq"] = statistics.mean([s["freq"] for s in next_scores])
        next_score["sha"] = next_sha
        bisect_params["before"] = sha
        bisect_params["after"] = next_sha
        bisect_options = "&".join([f"{k}={v}" for k, v in bisect_params.items()])
        next_score["bisect_link"] = f"{build_link}{bisect_options}"

    return flask.render_template('detail.html', scores=scores,
                                 prev_score=prev_score, next_score=next_score
                                 )

@app.route('/graph')
def flot_demo():
    """test deployment of flot"""
    scores = get_score_fields(g.cur, ["sha", "date", "summary", "author", "freq",
                                      "benchmark_name", "fps", "host_name", "kernel"],
                              "order by date")
    series = {}
    commits = {}
    frequencies = {}
    stdev = {}
    for score in scores:
        name = f"{score['host_name']}_{score['benchmark_name']}"
        if name not in series:
            series[name] = []
            frequencies[name] = []
            stdev[name] = []
        series[name].append(
            # python dates are in sec, java dates are in ms
            [score["date"] * 1000,
             score["fps"]])
        frequencies[name].append(score["freq"])
        stdev[name].append(0)
        score_date = datetime.datetime.fromtimestamp(score["date"]).ctime()
        commits[score["date"] * 1000] = {"date" : score_date,
                                         "author" : score["author"],
                                         "summary" : score["summary"],
                                         "sha" : score["sha"],
                                         "freq" : score["freq"],
                                         }
    data = { "points" : [{"label" : name, "data" : scores } for (name, scores) in series.items()],
             "commits" : commits,
             "frequencies" : frequencies,
             "stdev" : stdev }
    return flask.render_template('flot.html',
                                 page_title="all scores",
                                 data=data)

import multiprocessing
import os
import signal
import sys

import bjoern

from perforate import app

print("running majanes")

worker_pids = []

bjoern.listen(wsgi_app=app,
              host='0.0.0.0',
              port=8000,
              reuse_port=True)

for _ in range(int(multiprocessing.cpu_count()/2)):
    print("forking")
    pid = os.fork()
    if pid > 0:
        worker_pids.append(pid)
    elif pid == 0:
        try:
            print("running")
            bjoern.run()
        except KeyboardInterrupt:
            pass
        sys.exit()

try:
    for _ in worker_pids:
        print("waiting")
        os.wait()
except KeyboardInterrupt:
    print("interrupt")
    for pid in worker_pids:
        print("interrupt")
        os.kill(pid, signal.SIGINT)
